'use strict';

angular.module('constants', []);
angular.module('directives', []);
angular.module('services', ['ngCookies', 'constants']);
angular.module('controllers', ['services', 'ngRoute']);

angular.module('poneyracer', ['ngRoute', 'ngMessages', 'ui.bootstrap', 'controllers', 'directives'])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .when('/register', {
        templateUrl: 'app/register/register.html',
        controller: 'RegisterCtrl'
      })
      .when('/login', {
        templateUrl: 'app/login/login.html',
        controller: 'LoginCtrl'
      })
      .when('/races', {
        templateUrl: 'app/races/races.html',
        controller: 'RacesCtrl'
      })
      .when('/races/:raceId', {
        templateUrl: 'app/race/race.html',
        controller: 'RaceCtrl'
      })
      .when('/races/:raceId/live', {
        templateUrl: 'app/live/live.html',
        controller: 'LiveCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function(authenticationService) {
    authenticationService.init();
  })
;
