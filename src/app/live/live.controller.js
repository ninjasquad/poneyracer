'use strict';

angular.module('controllers')
  .controller('LiveCtrl', function($scope, $routeParams, racesService, config) {


    racesService.get($routeParams.raceId)
      .then(function(race) {
        $scope.race = race;
        $scope.bet = race.bettedPoney ? "You betted on " + race.bettedPoney : "You have no bet";
        return racesService.start($routeParams.raceId)
      })
      .finally(function() {
        var socket = new SockJS(config.server + '/race');
        var stompClient = Stomp.over(socket);

        stompClient.connect('', function() {
          stompClient.subscribe('/topic/' + $routeParams.raceId, function(message) {
            $scope.positions = angular.fromJson(message.body).positions;
            angular.forEach($scope.positions, function(position) {
              console.log(position);
              if (position.position === 100) {
                console.log("winner", position);
                $scope.winner = "The winner is " + position.poney + "!";
                $scope.bet = position.poney === $scope.race.bettedPoney ? "You win your bet!" : "You lost your bet...";
              }
            });
            $scope.$apply();
          });
        });
      });

  });
