'use strict';

describe('MainCtrl', function() {
  var scope;

  beforeEach(module('controllers'));

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should define more 2 users', inject(function($controller, $httpBackend, config) {
    // given a controller not yet initialized
    expect(scope.users).toBeUndefined();
    // and a backend with 2 users
    $httpBackend.expectGET(config.server + 'users')
      .respond([{login: 'ced'}, {login: 'jb'}]);

    // when initializing the controller
    $controller('MainCtrl', {
      $scope: scope
    });
    $httpBackend.flush();

    // then we should have 2 users on scope
    expect(angular.isArray(scope.users)).toBeTruthy();
    expect(scope.users.length).toBe(2);
  }));

  it('should logout a user', inject(function($controller, authenticationService) {
    // given
    spyOn(authenticationService, 'logout');
    $controller('MainCtrl', {
      $scope: scope,
      authenticationService: authenticationService
    });

    // when logging out
    scope.logout();

    // then we should have call the service
    expect(authenticationService.logout).toHaveBeenCalled();
  }));

  it('should check if a user is logged', inject(function($controller, authenticationService) {
    // given
    spyOn(authenticationService, 'isLogged');
    $controller('MainCtrl', {
      $scope: scope,
      authenticationService: authenticationService
    });

    // when checking if the user is logged
    scope.isLogged();

    // then we should have call the service
    expect(authenticationService.isLogged).toHaveBeenCalled();
  }));
});
