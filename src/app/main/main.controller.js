'use strict';

angular.module('controllers')
  .controller('MainCtrl', function($scope, $http, $location, authenticationService, config) {

    $scope.init = function() {
      $http.get(config.server + 'users')
        .success(function(data) {
          $scope.users = data;
        });
    };

    $scope.init();

    $scope.isLogged = function() {
      return authenticationService.isLogged();
    };

    $scope.logout = function() {
      authenticationService.logout();
    }

  });
