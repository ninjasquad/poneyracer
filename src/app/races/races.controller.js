'use strict';

angular.module('controllers')
  .controller('RacesCtrl', function($scope, racesService) {
    racesService.list()
      .then(function(data) {
        $scope.races = data;
      })
  });
