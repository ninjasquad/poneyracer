'use strict';

describe('RacesCtrl', function() {
  var scope;

  beforeEach(module('controllers'));

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should get the races list', inject(function($controller, $q, racesService) {
    // given a list of races
    var races = [{name: 'Race 1'}];
    spyOn(racesService, 'list').and.returnValue($q.when(races));

    // when initializing the controller
    $controller('RacesCtrl', {
      $scope: scope
    });
    scope.$apply();

    // then we should have 1 race on scope
    expect(scope.races.length).toBe(1);
  }));
});
