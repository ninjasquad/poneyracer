'use strict';

angular.module('services')
  .service('racesService', function racesService($http, $log, $q, config) {

    var list = function() {
      return $http.get(config.server + 'races')
        .then(function(response) {
          return response.data;
        }, function(data) {
          $log.error(data);
          return $q.reject();
        });
    };

    var getOne = function(id) {
      return $http.get(config.server + 'races/' + id)
        .then(function(response) {
          return response.data;
        }, function(data) {
          $log.error(data);
          return $q.reject();
        });
    };

    var bet = function(race, poney) {
      return $http.post(config.server + 'bets', {raceId: race, poney: poney})
        .then(function(response) {
          return response.data;
        }, function(data) {
          $log.error(data);
          return $q.reject();
        });
    };

    var cancelBet = function(race) {
      return $http.delete(config.server + 'bets/' + race)
        .then(function(response) {
          return response.data;
        }, function(data) {
          $log.error(data);
          return $q.reject();
        });
    };

    var start = function(race){
      return $http.post(config.server + 'running', race)
        .then(function(response) {
          return response.data;
        }, function(data) {
          $log.error(data);
          return $q.reject();
        });
    };

    return {
      list: list,
      get: getOne,
      bet: bet,
      cancelBet: cancelBet,
      start: start
    };
  });
