'use strict';

angular.module('services')
  .service('authenticationService', function authenticationService($http, $cookies, $log, $q, config) {

    var login = function(credentials) {
      return $http.post(config.server + 'authentication', credentials)
        .then(function(response) {
          $cookies.put('customAuth', response.data.token);
          init();
        }, function(data) {
          $log.error(data);
          return $q.reject();
        });
    };

    var init = function() {
      $http.defaults.headers.common['Custom-Authentication'] = $cookies.get('customAuth');
    };

    var isLogged = function() {
      return angular.isDefined($cookies.get('customAuth'));
    };

    var logout = function() {
      $cookies.remove('customAuth');
    };

    return {
      login: login,
      init: init,
      isLogged: isLogged,
      logout: logout
    };
  });
