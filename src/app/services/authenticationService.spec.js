'use strict';

describe('Service: AuthenticationService', function() {

  // load the service's module
  beforeEach(module('services'));

  // instantiate service
  var authenticationService;

  beforeEach(inject(function(_authenticationService_) {
    authenticationService = _authenticationService_;
  }));

  it('should login', inject(function($cookies, $httpBackend, $http, config) {
    // given correct credentials
    var credentials = {
      login: 'ced',
      password: 'password'
    };
    $httpBackend.expectPOST(config.server + 'authentication', credentials)
      .respond({token: 'ced'});

    // when login is called
    authenticationService.login(credentials);
    $httpBackend.flush();

    // then we should a have a cookie
    expect($cookies.get('customAuth')).toBe('ced');
    // a custom http header
    expect($http.defaults.headers.common['Custom-Authentication']).toBe('ced');
  }));

  it('should tell us if a user is logged in', inject(function($cookies) {
    // given a cookie set
    $cookies.put('customAuth', 'ced');

    // then we should have true when isLogged is called
    expect(authenticationService.isLogged()).toBe(true);
  }));

  it('should tell us if a user is not logged in', inject(function($cookies) {
    // given no cookie set
    $cookies.remove('customAuth');

    // then we should have true when isLogged is called
    expect(authenticationService.isLogged()).toBe(false);
  }));

  it('should logout a logged in user', inject(function($cookies) {
    // given a cookie set
    $cookies.put('customAuth', 'ced');

    // when we log the user out
    authenticationService.logout();

    // then we should have no cookies
    expect($cookies.get('customAuth')).toBeUndefined();
  }));


});
