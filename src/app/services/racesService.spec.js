'use strict';

describe('Service: RacesService', function() {

  // load the service's module
  beforeEach(module('services'));

  // instantiate service
  var racesService;

  beforeEach(inject(function(_racesService_) {
    racesService = _racesService_;
  }));

  it('should list races', inject(function($httpBackend, $http, config) {
    // given a list of races
    var races = [{name: 'Race 1'}];
    $httpBackend.expectGET(config.server + 'races')
      .respond(races);

    // when list is called
    racesService.list();
    $httpBackend.flush();
  }));

  it('should get a race', inject(function($httpBackend, $http, config) {
    // given a list of races
    var race = {name: 'Race 1'};
    $httpBackend.expectGET(config.server + 'races/1')
      .respond(race);

    // when get is called
    racesService.get(1);
    $httpBackend.flush();
  }));

  it('should bet on a poney', inject(function($httpBackend, $http, config) {
    // given
    $httpBackend.expectPOST(config.server + 'bets')
      .respond(200);

    // when bet is called
    racesService.bet(1, 'Blue');
    $httpBackend.flush();
  }));

  it('should cancel a bet on a poney', inject(function($httpBackend, $http, config) {
    // given
    $httpBackend.expectDELETE(config.server + 'bets/1')
      .respond(200);

    // when cancel is called
    racesService.cancelBet(1);
    $httpBackend.flush();
  }));

  it('should start a race', inject(function($httpBackend, $http, config) {
    // given
    $httpBackend.expectPOST(config.server + 'running')
      .respond(200);

    // when start is called
    racesService.start(1);
    $httpBackend.flush();
  }));

});
