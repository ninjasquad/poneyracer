'use strict';

angular.module('controllers')
  .controller('LoginCtrl', function($scope, $location, authenticationService) {

    $scope.login = function(credentials) {
      authenticationService.login(credentials)
        .then(function() {
          $location.path('/');
        });
    }

  });
