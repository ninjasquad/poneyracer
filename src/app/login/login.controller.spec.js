'use strict';

describe('LoginCtrl', function() {
  var scope;

  beforeEach(module('controllers'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    $controller('LoginCtrl', {
      $scope: scope
    });
  }));

  it('should log in', inject(function(authenticationService, $location, $q) {
    // given a backend with a user
    spyOn(authenticationService, 'login').and.returnValue($q.when());
    spyOn($location, 'path');
    var credentials = {
      login: 'cedric',
      password: 'cedric'
    };

    // when we try to log in
    scope.login(credentials);
    scope.$apply(); // this is needed to resolve the promise

    // then we should have redirect the user
    expect($location.path).toHaveBeenCalledWith('/');
  }));
});
