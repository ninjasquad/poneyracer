'use strict';

angular.module('directives')
  .directive('poney', function() {
    return {
      template: '<div><img ng-class="{green: betted}" ng-src="assets/images/poney-{{ name | lowercase }}.png" alt="{{ name }}"></img></div>',
      restrict: 'E',
      scope: {
        name: '@',
        betted: '='
      }
    };
  });
