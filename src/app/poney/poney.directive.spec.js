'use strict';

describe('Poney Directive', function() {
  var scope, poney;

  beforeEach(module('directives'));

  beforeEach(inject(function($rootScope) {
    scope = $rootScope;
    scope.poney = 'Blue';
  }));

  it('should insert the correct img', inject(function($compile) {
    // when we compile the directive
    poney = $compile('<poney name="{{ poney }}"></poney>')(scope);
    scope.$digest();

    // then we should have an image
    var img = poney.find('div').children()[0];
    expect(img.tagName).toBe('IMG');
    expect(angular.element(img).attr('src')).toBe('assets/images/poney-blue.png');
  }));

  it('should add a border if the poney is the betted one', inject(function($compile) {
    // given that the betted poney is the blue one
    scope.betted = 'Blue';

    // when we compile the directive
    poney = $compile('<poney name="{{ poney }}" betted="poney === betted"></poney>')(scope);
    scope.$digest();

    // then we should have a border
    var img = poney.find('div').children()[0];
    expect(angular.element(img).attr('class')).toContain('green');
  }));
});
