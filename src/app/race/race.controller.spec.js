'use strict';

describe('RaceCtrl', function() {
  var scope;

  beforeEach(module('controllers'));

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should get the race', inject(function($controller, $q, $routeParams, racesService) {
    // given a race
    var race = {name: 'Race 1'};
    spyOn(racesService, 'get').and.returnValue($q.when(race));

    // when initializing the controller
    $routeParams.raceId = 1;
    $controller('RaceCtrl', {
      $scope: scope
    });
    scope.$apply();

    // then we should have 1 race on scope
    expect(scope.race.name).toBe(race.name);
  }));


  it('should bet on a poney', inject(function($controller, $q, $routeParams, racesService) {
    // given a race
    var race = {name: 'Race 1'};
    spyOn(racesService, 'get').and.returnValue($q.when(race));
    spyOn(racesService, 'bet').and.returnValue($q.when());

    // when betting
    $routeParams.raceId = 1;
    $controller('RaceCtrl', {
      $scope: scope
    });
    scope.bet('Blue');
    scope.$apply();

    // then we should have refresh the view
    expect(racesService.bet).toHaveBeenCalledWith(1, 'Blue');
    expect(racesService.get).toHaveBeenCalledWith(1);
  }));

  it('should cancel a bet on a poney', inject(function($controller, $q, $routeParams, racesService) {
    // given a race
    var race = {name: 'Race 1'};
    spyOn(racesService, 'get').and.returnValue($q.when(race));
    spyOn(racesService, 'cancelBet').and.returnValue($q.when());

    // when canceling a bet
    $routeParams.raceId = 1;
    $controller('RaceCtrl', {
      $scope: scope
    });
    scope.cancelBet();
    scope.$apply();

    // then we should have refresh the view
    expect(racesService.cancelBet).toHaveBeenCalledWith(1);
    expect(racesService.get).toHaveBeenCalledWith(1);
  }));
});
