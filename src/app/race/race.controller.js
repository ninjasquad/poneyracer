'use strict';

angular.module('controllers')
  .controller('RaceCtrl', function($scope, $routeParams, racesService) {
    $scope.show = function() {
      racesService.get($routeParams.raceId)
        .then(function(data) {
          $scope.race = data;
        });
    };

    $scope.show();

    $scope.bet = function(poney) {
      racesService.bet($routeParams.raceId, poney)
        .then($scope.show);
    };

    $scope.cancelBet = function() {
      racesService.cancelBet($routeParams.raceId)
        .then($scope.show);
    }
  });
