'use strict';

describe('RegisterCtrl', function() {
  var scope, $log, $location;

  beforeEach(module('controllers'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    $log = jasmine.createSpyObj('$log', ['debug', 'error']);
    $location = jasmine.createSpyObj('$location', ['path']);

    $controller('RegisterCtrl', {
      $scope: scope,
      $log: $log,
      $location: $location
    });
  }));

  it('should define a max birth year', function() {
    // when
    var maxBirthYear = scope.maxBirthYear();

    // then we should have a year > 1996
    expect(maxBirthYear).toBeGreaterThan(1996);
  });

  it('should return true if a field is invalid and dirty', function() {
    // given an invalid form field
    var field = {
      $invalid: true,
      $dirty: false
    };

    // then
    expect(scope.isInvalidAndDirty(field)).toBeFalsy();

    // given a field dirty and invalid
    field.$dirty = true;

    // then
    expect(scope.isInvalidAndDirty(field)).toBeTruthy();

    // given a field dirty only
    field.$invalid = false;

    // then
    expect(scope.isInvalidAndDirty(field)).toBeFalsy();
  });

  it('should register a user and redirect', inject(function($httpBackend, config) {
    // given a user to register
    var user = {firstName: 'cedric'};

    // and a backend expecting a registration
    $httpBackend.expectPOST(config.server + 'users')
      .respond({token: 'cedric'});

    // when registering a user
    scope.register(user);
    $httpBackend.flush();

    // then we should have log the success and redirect
    expect($log.debug).toHaveBeenCalled();
    expect($location.path).toHaveBeenCalledWith('/');
  }));

  it('should log an error if the registration fails', inject(function($httpBackend, config) {
    // given a user to register
    var user = {firstName: 'cedric'};

    // and a backend expecting a registration
    $httpBackend.expectPOST(config.server + 'users')
      .respond(400);

    // when registering a user
    scope.register(user);
    $httpBackend.flush();

    // then we should have log the error
    expect($log.error).toHaveBeenCalled();
  }));
});
