'use strict';

angular.module('controllers')
  .controller('RegisterCtrl', function($scope, $http, $log, $location, config) {

    $scope.isInvalidAndDirty = function(field) {
      return field.$invalid && field.$dirty;
    };

    $scope.maxBirthYear = function() {
      return new Date().getFullYear() - 18;
    };

    $scope.register = function(user) {
      $http.post(config.server + 'users', user)
        .success(function(data) {
          $log.debug('user registered!', data.token);
          $location.path('/');
        })
        .error(function(error) {
          $log.error('error on registration', error);
        });
    };

  });
