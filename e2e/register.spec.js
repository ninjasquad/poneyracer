describe('Register', function() {
  it('should display the login field', function() {
    browser.get('#register');

    var loginLabel = $('#login-label');
    var login = $('#login');
    var loginWarning = $('#login-warning');

    // when field is empty
    expect(loginLabel.getText()).toBe('Login');
    expect(loginLabel.getAttribute('class')).toBe('');
    expect(login.getAttribute('class')).not.toContain('ng-invalid-minlength');
    expect(login.getCssValue('border')).toContain('');
    expect(loginWarning.isPresent()).toBeFalsy();

    // typing one character should trigger invalid-length
    login.sendKeys('c');

    // it should now be invalid
    // with label red
    expect(loginLabel.getAttribute('class')).toBe('red');
    // and field with orange border
    expect(login.getAttribute('class')).toContain('ng-invalid-minlength');
    expect(login.getCssValue('border')).toContain('3px solid rgb(255, 165, 0)');
    // and warning displayed
    expect(loginWarning.isDisplayed()).toBeTruthy();
    expect(loginWarning.getText()).toBe('Your login should have at least 2 characters');
    // typing a second character should make it valid
    login.sendKeys('e');

    // it should now be valid
    expect(loginLabel.getAttribute('class')).toBe('');
    // and field with no border
    expect(login.getAttribute('class')).not.toContain('ng-invalid-minlength');
    expect(login.getCssValue('border')).toContain('');
    // and warning displayed
    expect(loginWarning.isPresent()).toBeFalsy();
  });

  it('should display the submit button', function() {
    browser.get('#register');

    var submit = $('#submit');

    // should not be clickable
    expect(submit.isEnabled()).toBeFalsy();

    // fill all fields
    $('#firstname').sendKeys('Cédric');
    $('#lastname').sendKeys('Exbrayat');
    $('#birthyear').sendKeys('1986');
    $('#email').sendKeys('cedric@ninja-squad.com');
    $('#login').sendKeys('ced');
    $('#password').sendKeys('password');

    // should now be clickable
    expect(submit.isEnabled()).toBeTruthy();
  });
});
